import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { MatSort, MatTableDataSource } from '@angular/material';

import Data from '../../../assets/data.json';
import { ListEntry } from 'src/app/models/ListEntry.model.js';

const listData: ListEntry[] = Data;

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  displayedColumns: string[] = ['city', 'start_date', 'end_date', 'price', 'status', 'color'];
  dataSource = new MatTableDataSource(listData);

  @ViewChild(MatSort) sort: MatSort;
  /** Reference of the start date input */
  @ViewChild('startDateInput') startDateInput: ElementRef;
  /** Reference of the end date input */
  @ViewChild('endDateInput') endDateInput: ElementRef;

  ngOnInit() {
    this.dataSource.sort = this.sort;

    /** Handle date fields as Date while sorting */
    this.dataSource.sortingDataAccessor = (entry, property) =>
      (property === 'start_date' || property === 'end_date') ?
        new Date(entry[property]) : entry[property];
  }

  /**
   * Filter listData by the selected date values.
   * If one of the values are empty, compare with the current date.
   */
  filterByDate(): void {
    const selectedStartDate = new Date(this.startDateInput.nativeElement.value || Date.now());
    const selectedEndDate = new Date(this.endDateInput.nativeElement.value || Date.now());

    const filteredData = listData.filter((entry) => {
      return new Date(entry.start_date) >= selectedStartDate && new Date(entry.end_date) <= selectedEndDate;
    });

    this.dataSource.data = (filteredData.length) ? filteredData : listData;
  }
}
