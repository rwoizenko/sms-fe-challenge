import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {
  signupForm: FormGroup;

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.signupForm = this.formBuilder.group({
      email: this.formBuilder.control(null, [Validators.required, Validators.email]),
      password: this.formBuilder.control(null, [Validators.required, Validators.minLength(8)]),
      confirmPassword: this.formBuilder.control(null, [Validators.required, this.passwordMatchValidator.bind(this)])
    });
  }

  /* Check if passwords are matching */
  passwordMatchValidator(control: FormControl): object | null {
    if (this.signupForm) {
      const password = this.signupForm.get('password').value;
      if (password === control.value) {
        return null;
      }
    }
    return {noPasswordMatch: true};
  }

  submitForm() {
    this.signupForm.controls.confirmPassword.updateValueAndValidity();

    if (this.signupForm.valid) {
      alert('Your registration was successful.\n Please check your mail account.');
    }
  }
}
